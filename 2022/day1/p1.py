# https://adventofcode.com/2022/day/1
from typing import List

data = """
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
"""


class Elf:
    elf_num: int = 0
    calories: List[int] = []
    _sum: int = 0

    def __init__(self, elf_num: int, calories: List[int]):
        self.elf_num = elf_num
        self.calories = calories

    def sum(self):
        return sum(self.calories)


army: List[Elf] = []
buffer: List[int] = []
current_elf: int = 0
for line in data.splitlines():
    if line.strip() == "":
        # if current line is empty, append the elf with current buffer to army
        army.append(Elf(current_elf, buffer))
        current_elf += 1
        buffer = []
        continue
    # build the buffer with current line, convert to int and strip any whitespaces
    buffer.append(int(line.strip()))

fattest_elf: Elf = None
elf: Elf
for elf in army:
    if fattest_elf is None or elf.sum() > fattest_elf.sum():
        fattest_elf = elf

print(fattest_elf.elf_num, fattest_elf.sum())
